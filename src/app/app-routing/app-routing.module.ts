import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../pages/home/home-page.component';
import { LoginComponent } from '../pages/login/login-page.component';
import { ReportPageComponent } from '../pages/report-page/report-page.component';
import { RequestPageComponent } from '../pages/request-page/request-page.component';
import { LogoutComponent } from '../components/logout/logout.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  {path: 'home', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'report', component: ReportPageComponent},
  {path: 'request', component: RequestPageComponent},
  {path: 'logout', component: LogoutComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
