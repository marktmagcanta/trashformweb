import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

/** FIREBASE DATABASE */
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from 'src/environments/environment';
import { AngularFireAuthModule } from 'angularfire2/auth';


/** Angular Material */
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule, } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import {MatInputModule, MatToolbarModule, MatListModule} from '@angular/material';

/** ROUTER */
import { AppRoutingModule } from './app-routing/app-routing.module';

/** FORMS */
import { FormsModule } from '@angular/forms';

/** COMPONENTS */
import { HomeComponent } from './pages/home/home-page.component';
import { LoginComponent } from './pages/login/login-page.component';
import { ReportPageComponent } from './pages/report-page/report-page.component';
import { RequestPageComponent } from './pages/request-page/request-page.component';
import { LogoutComponent } from './components/logout/logout.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    ReportPageComponent,
    RequestPageComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    AppRoutingModule,
    AngularFireAuthModule,
    FormsModule,
    MatToolbarModule,
    MatListModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
