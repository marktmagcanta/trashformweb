import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from 'firebase';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginComponent implements OnInit {

  /** USER INPUT */
  email: string;
  password: string;

  constructor(private database: AngularFireDatabase, private auth: AngularFireAuth, private router: Router) { }

  ngOnInit() {
  }

  async login() {
    const res = await this.auth.auth.signInWithEmailAndPassword(this.email, this.password);

    console.log(res);
    if (res) {
      this.router.navigate(['home']);
    } else {
      console.log('Error');
    }
  }

}
