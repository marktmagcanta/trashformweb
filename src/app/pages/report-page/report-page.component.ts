import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-report-page',
  templateUrl: './report-page.component.html',
  styleUrls: ['./report-page.component.css']
})
export class ReportPageComponent implements OnInit {

  reportRef$: AngularFireList<any>;
  reports: Observable<any[]>;

  constructor(private dabatase: AngularFireDatabase) {
    this.reportRef$ = this.dabatase.list('REPORT');
  }

  ngOnInit() {
    this.reports = this.reportRef$.valueChanges();
  }

  data(data): void {
    console.log(data);
  }

}
