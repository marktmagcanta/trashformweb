import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-request-page',
  templateUrl: './request-page.component.html',
  styleUrls: ['./request-page.component.css']
})
export class RequestPageComponent implements OnInit {

  requestRef$: AngularFireList<any>;
  requests: Observable<any[]>;

  constructor(private database: AngularFireDatabase) {
    this.requestRef$ = this.database.list<any>(`REQUEST`, ref => ref.orderByChild('timestamp'));
  }

  ngOnInit() {
    this.requests = this.requestRef$.valueChanges();
  }

}
